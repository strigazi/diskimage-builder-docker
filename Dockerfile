FROM ubuntu:14.04

RUN apt-get update -y && \
    apt-get install -y python-dev build-essential python-pip kpartx \
                       python-lzma qemu-utils yum yum-utils python-yaml \
                       git uuid-runtime parted


RUN pip install diskimage-builder

RUN mkdir -p /opt/stack
RUN git clone https://git.openstack.org/openstack/magnum /opt/stack/magnum
RUN git clone https://git.openstack.org/openstack/diskimage-builder /opt/stack/diskimage-builder

ADD execute.sh /opt/stack/execute.sh
