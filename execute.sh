#!/bin/sh


export DIB_ELEMENTS=/opt/stack/diskimage-builder/elements
export MAGNUM_ELEMENTS=/opt/stack/magnum/magnum/drivers/common/image
export ELEMENTS_PATH=$DIB_ELEMENTS:$MAGNUM_ELEMENTS
export DIB_RELEASE=23
export DIB_IMAGE_SIZE=2.5
export DIB_DEBUG_TRACE=1

disk-image-create -o fedora-atomic-23 -t qcow2 fedora-atomic
